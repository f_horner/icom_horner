<!doctype html>
<!---
Je ne maîtrise pas le php puisque je ne suis pas un informaticien de base.
Toutes les commandes mysql figurent dans le fichier config.php comme dans
l'exemple fourni en classe. Cela tourne en partie mais sans plus, je ne peux
faire mieux.
-->

<?php require_once('inc/config.php'); ?>

<?php
$stmt = $db->prepare($q_select);
$stmt->execute();
$tasks= $stmt->fetchAll();
?>

<html class="no-js" lang="en">
  <body>
        <h1>Projet Manager</h1>
        <?php require_once('calendar.php');?>
  </body>
</html>
